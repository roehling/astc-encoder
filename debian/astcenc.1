.TH ASTCENC 1
.SH NAME
astcenc \- compress or decompress images using the ASTC format
.SH SYNOPSIS
.SY astcenc
.RB [ \-cl | \-cs | \-ch | \-cH ]
.I <in> <out> <blocksize> <preset>
.RI [ options ]
.YS
.SY astcenc
.RB [ \-dl | \-ds | \-dh | \-dH ]
.I <in> <out> <blocksize> <preset>
.RI [ options ]
.YS
.SY astcenc
.RB [ \-tl | \-ts | \-th | \-tH ]
.I <in> <out> <blocksize> <preset>
.RI [ options ]
.YS
.SH DESCRIPTION
.B astcenc
compresses image files into the Adaptive Scalable Texture Compression (ASTC)
image format, a lossy compression format design for use in real-time graphics
applications. It is a fully featured compressor implementation, supporting all
of the compression profiles and block sizes specified by the ASTC format:
.IP \(bu 4
All color profiles (LDR linear, LDR sRGB, and HDR)
.IP \(bu 4
All 2D block sizes (4x4 through to 12x12)
.IP \(bu 4
All 3D block sizes (3x3x3 through to 6x6x6)
.PP
The compressor provides a number of pre-determined quality presets, which allow
users to tradeoff compressed image quality against compression performance. For
advanced users the compressor provides many additional control options.
.PP
.B astcenc
can also be used to decompress ASTC compressed images, and perform compression
image quality analysis.
.SH COMPRESSION
To compress an image using the ASTC format you must specify the
color profile, the input file name, the output file name, the
target block size, and the quality preset.
.PP
The color profile is specified using the
.BR \-cl " (LDR linear),"
.BR \-cs " (LDR sRGB),"
.BR \-ch " (HDR RGB, LDR A),"
or
.BR \-cH "(HDR RGBA)"
encoder options. Note that not all hardware implementations of ASTC support
the HDR profile.
.PP
The input file path must match a valid file format for compression, and the
output file format must be a valid output for compression.  See the FILE
FORMATS section for the list of supported formats.
.PP
The block size must be a valid ASTC block size. Every block compresses into 128
bits of compressed output, so the block size determines the compressed data
bitrate.
.PP
Supported 2D block sizes are:
.IP \(bu 4
4x4: 8.00 bpp
.IP \(bu 4
5x4: 6.40 bpp
.IP \(bu 4
5x5: 5.12 bpp
.IP \(bu 4
6x5: 4.27 bpp
.IP \(bu 4
6x6: 3.56 bpp
.IP \(bu 4
8x5: 3.20 bpp
.IP \(bu 4
8x6: 2.67 bpp
.IP \(bu 4
10x5: 2.56 bpp
.IP \(bu 4
10x6: 2.13 bpp
.IP \(bu 4
8x8: 2.00 bpp
.IP \(bu 4
10x8: 1.60 bpp
.IP \(bu 4
10x10: 1.28 bpp
.IP \(bu 4
12x10: 1.07 bpp
.IP \(bu 4
12x12: 0.89 bpp
.PP
Supported 3D block sizes are:
.IP \(bu 4
3x3x3: 4.74 bpp
.IP \(bu 4
4x3x3: 3.56 bpp
.IP \(bu 4
4x4x3: 2.67 bpp
.IP \(bu 4
4x4x4: 2.00 bpp
.IP \(bu 4
5x4x4: 1.60 bpp
.IP \(bu 4
5x5x4: 1.28 bpp
.IP \(bu 4
5x5x5: 1.02 bpp
.IP \(bu 4
6x5x5: 0.85 bpp
.IP \(bu 4
6x6x5: 0.71 bpp
.IP \(bu 4
6x6x6: 0.59 bpp
.PP
The quality preset configures the quality-performance tradeoff for the
compressor; more complete searches of the search space improve image quality at
the expense of compression time. The available presets are:
.IP
.B \-fastest
.br
.B \-fast
.br
.B \-medium
.br
.B \-thorough
.br
.B \-exhaustive
.PP
Using the 
.B \-fastest
setting throws away a lot of image quality compared. It is useful for quickly
roughing-out new content, but we recommend using higher quality settings for
production builds.
.PP
Using the
.B \-exhaustive
setting significantly increases compression time, but typically only gives
minor quality improvements over using
.BR \-thorough .
.PP
There are a number of additional compressor options which are useful to
consider for common usage, based on the type of image data being compressed.
.TP
.B \-mask
The input texture is a mask texture with unrelated data stored in the various
color channels, so enable error heuristics that aim to improve perceptual
quality by minimizing the effect of error cross-talk across the color channels.
.TP
.B \-normal
The input texture is a three channel linear LDR normal map storing unit length
normals as (R=X, G=Y, B=Z). The output will be a two channel X+Y normal map
stored as (RGB=X, A=Y), optimized for angular error instead of simple PSNR. The
Z component can be recovered programmatically in shader code by using the
equation:
.IP
nml.xy = texture(...).ga;              // Load in [0,1]
.IP
nml.xy = nml.xy * 2.0 - 1.0;           // Unpack to [-1,1]
.IP
nml.z = sqrt(1 - dot(nml.xy, nml.xy)); // Compute Z
.TP
.B \-perceptual
The codec should optimize perceptual error, instead of direct RMS error. This
aims to improves perceived image quality, but typically lowers the measured
PSNR score. Perceptual methods are currently only available for normal maps.
.TP
.BR -array " " \fI<size>\fR
Loads an array of
.I <size>
2D image slices to use as a 3D image.  The input filename given is used is
decorated with the postfix "_<slice>" to find the file to load. For example, an
input named "input.png" would load as input_0.png, input_1.png, etc.
.TP
.B \-pp\-normalize
Run a preprocess over the image that forces normal vectors to be unit length.
Preprocessing applies before any codec encoding swizzle, so normal data must be
in the RGB channels in the source image.
.TP
.B \-pp\-premultiply
Run a preprocess over the image that scales RGB components in the image by the
alpha value. Preprocessing applies before any codec encoding swizzle, so color
data must be in the RGB channels in the source image.
.SH COMPRESSION TIPS & TRICKS
ASTC is a block-based format that can be prone to block artifacts.  If block
artifacts are a problem when compressing a given texture, adding some or all of
following command-line options may help:
.IP
.BR \-b " 1.8"
.br
.BR \-v " 2 1 1 0 25 0.1"
.br
.BR \-va " 1 1 0 25"
.br
.BR \-dblimit " 60"
.PP
The
.B \-b
option is a general-purpose block-artifact reduction option.
The
.B \-v
and
.B -va
option settings will concentrate effort where smooth regions lie next to
regions with high detail, which are particularly prone to block artifacts.
Increasing the
.B -dblimit
option is sometimes also needed to force the compressor to keep searching for a
better encoding, which can be needed in images with smooth gradients.
.PP
If a texture exhibits severe block artifacts in only some of the color
channels, which is a common problem for mask textures, then using the
.B \-cw
option to raise the weighting of the affected color channel(s) may help. For
example, if the green color channel is particularly badly encoded then try
.BR \-cw " 1 6 1 1."
.SH ADVANCED COMPRESSION
.SS Error weighting options
These options provide low-level control of the codec error metric
computation, used to determine what good compression looks like.
.TP
.IR \fB\-v\fR " " <radius> " " <power> " " <base> " " <mean> " " <stdev> " " <mix>
Compute the per-texel relative error weighting for the RGB color
channels as follows:
.IP
weight = 1 / (\fI<base>\fR + \fI<mean>\fR * \fI<mean>\fR + \fI<stdev>\fR * \fI<stdev>\fR)
.IP
The
.I <radius>
argument specifies the texel radius of the
neighborhood over which the average and standard deviation are
computed.
.PP
The
.I <mix>
parameter is used to control the degree of mixing
of the average and stddev error components across the color
channels. Setting this parameter to 0 causes the computation
to be done completely separately for each color channel; setting
it to 1 causes the results from the RGB channels to be combined
and applied to all three together. Intermediate values between
these two extremes do a linear mix of the two error values.
.PP
The
.I <power>
argument is a power used to raise the values of the
input texels before computing average and standard deviation;
e.g. a power of 0.5 causes the codec to take the square root
of every input texel value.
.TP
.IR \fB\-va\fR " " <power> " " <base> " " <mean> " " <stdev>
Compute the per-texel relative error weighting for the alpha
channel, when used in conjunction with
.BR \-v .
See documentation for
.B \-v
for parameter documentation.
.TP
.IR \fB\-a\fR " " <radius>
For textures with alpha channel, scale per-texel weights by the
alpha value. The alpha value chosen for scaling of any
particular texel is taken as an average across a neighborhood
of the texel defined by the <radius> argument. Setting
.I <radius>
to 0 causes only the texel's own alpha to be used.
.TP
.IR \fB\-cw\fR " " <red> " " <green> " " <blue> " " <alpha>
Assign an additional weight scaling to each color channel,
allowing the channels to be treated differently in terms of
error significance. Set values above 1 to increase a channel's
significance, and values below 1 to decrease it. Set to 0
to exclude a channel from error computation completely.
.TP
.IR \fB\-b\fR " "<weight>
Assign an additional weight scaling for texels at compression
block edges and corners. Setting this to a value above 1
increases the significance of texels closer to the edges of a
block, and can help to reduce block artifacts.
.TP
.IR \fB-mpsnr\fR " " <low> " " <high>
Set the low and high f-stop values for the mPSNR error metric.
The mPSNR error metric only applies to HDR textures.
.SS Performance-quality tradeoff options
These options provide low-level control of the codec heuristics that
drive the performance-quality trade off.
.TP
.IR \fB\-partitionlimit\fR " " <number>
Test only
.I <number>
block partitions. Higher numbers give
better quality, however large values give diminishing returns
especially for smaller block sizes. Preset defaults are:
.IP
\-fastest    :    2
.br
\-fast       :    4
.br
\-medium     :   25
.br
\-thorough   :  100
.br
\-exhaustive : 1024
.TP
.IR \fB\-blockmodelimit\fR " " <number>
Test only block modes below the <number> usage centile in an
empirically determined distribution of block mode frequency.
This option is ineffective for 3D textures. Preset defaults are:
.IP
\-fastest    :  25
.br
\-fast       :  50
.br
\-medium     :  75
.br
\-thorough   :  95
.br
\-exhaustive : 100
.TP
.IR \fB\-refinementlimit\fR " " <value>
Iterate only
.I <value>
refinement iterations on colors and
weights. Minimum value is 1. Preset defaults are:
.IP
\-fastest    : 1
.br
\-fast       : 1
.br
\-medium     : 2
.br
\-thorough   : 4
.br
\-exhaustive : 4
.TP
.IR \fB\-candidatelimit\fR " " <value>
Trial only 
.I <value>
candidate encodings for each block mode:
.TP
\-fastest    : 1
.br
\-fast       : 2
.br
\-medium     : 2
.br
\-thorough   : 3
.br
\-exhaustive : 4
.TP
.IR \fB\-dblimit\fR " " <number>
Stop compression work on a block as soon as the PSNR of the
block, measured in dB, exceeds
.IR <number> .
This option is
ineffective for HDR textures. Preset defaults, where N is the
number of texels in a block, are:
.IP
\-fastest    : MAX(53-19*log10(N),  70-35*log10(N))
.br
\-fast       : MAX(63-19*log10(N),  85-35*log10(N))
.br
\-medium     : MAX(70-19*log10(N),  95-35*log10(N))
.br
\-thorough   : MAX(77-19*log10(N), 105-35*log10(N))
.br
\-exhaustive : 999
.TP
.IR \fB\-partitionearlylimit\fR " " <factor>
Stop compression work on a block after only testing blocks with
up to two partions and one plane of weights, unless the two
partition error term is lower than the error term from encoding
with one partition by more than the specified factor. This
option is ineffective for normal maps. Preset defaults are:
.IP
\-fastest    :    1.0
.br
\-fast       :    1.0
.br
\-medium     :    1.2
.br
\-thorough   :    2.5
.br
\-exhaustive : 1000.0
.TP
.IR \fB\-planecorlimit\fR " " <factor>
Stop compression after testing only one planes of weights,
unless the minimum color correlation factor between any pair of
color channels is below this factor. This option is ineffective
for normal maps. Preset defaults are:
.IP
\-fastest    : 0.50
.br
\-fast       : 0.50
.br
\-medium     : 0.75
.br
\-thorough   : 0.95
.br
\-exhaustive : 0.99
.SS Other options
.TP
.IR \fB\-esw\fR " " <swizzle>
Swizzle the color components before compression. The swizzle is
specified using a 4-character string, which defines the output
format ordering. The characters may be taken from the set
[rgba01], selecting either input color channels or a literal
zero or one. For example to swap the RG channels, and replace
alpha with 1, the swizzle 'grb1' should be used.
.IP
The input swizzle takes place before any compression, and all
error weighting applied using the -cw option is applied to the
post-swizzle channel ordering.
.IP
By default all 4 post-swizzle channels are included in the error
metrics during compression. When using -esw to map two channel
data to the L+A endpoint (e.g. -esw rrrg) the luminance data
stored in the rgb channels will be weighted three times more
strongly than the alpha channel. This can be corrected using the
.B \-cw
option to zero the weights of unused channels; e.g. using
.BR \-cw " 1 0 0 1."
.TP
.IR \fB\-dsw\fR " " <swizzle>
Swizzle the color components after decompression. The swizzle is
specified using the same method as the
.B -esw
option, with support
for an additional "z" character. This is used to specify that
the compressed data stores an X+Y normal map, and that the Z
output channel should be reconstructed from the two channels
stored in the data. For the typical ASTC normal encoding,
which uses an 'rrrg' compression swizzle, you should specify an 'raz1'
swizzle for decompression.
.TP
.B \-yflip
Flip the image in the vertical axis prior to compression and
after decompression. Note that using this option in a test mode
(-t*) will have no effect as the image will be flipped twice.
.TP
.IR \fB\-j\fR " " <threads>
Explicitly specify the number of compression/decompression
theads to use in the codec. If not specified, the codec will
use one thread per CPU detected in the system.
.TP
.B \-silent
Suppresses all non-essential diagnostic output from the codec.
Error messages will always be printed, as will mandatory outputs
for the selected operation mode. For example, the test mode
will always output image quality metrics and compression time
but will suppress all other output.
.SH DECOMPRESSION
To decompress an image stored in the ASTC format you must specify
the color profile, the input file name, and the output file name.
.PP
The color profile is specified using the
.BR \-dl " (LDR linear),"
.BR \-ds " (LDR sRGB),"
.BR \-dh " (HDR RGB, LDR A),"
or
.BR \-dH "(HDR RGBA)"
decoder options.
.PP
The input file path must match a valid file format for
decompression, and the output file format must be a valid output for
a decompressed image. Note that not all output formats that the
coompression path can produce are supported for decompression. See
the FILE FORMATS section for the list of supported formats.
.PP
The
.B -dsw
options documented in ADVANCED COMPRESSION option documentation
are relevant to decompression.
.SH TEST
To perform a compression test which round-trips a single image
through compression and decompression and stores the decompressed
result back to file, you must specify same settings as COMPRESSION
other than swapping the color profile to select test mode. Note that
the compressed intermediate data is discarded in this mode.
.PP
The color profile is specified using the
.BR \-tl " (LDR linear),"
.BR \-ts " (LDR sRGB),"
.BR \-th " (HDR RGB, LDR A),"
or
.BR \-tH " (HDR RGBA)"
encoder options.
.PP
This operation mode will print error metrics suitable for either
LDR and HDR images, allowing some assessment of the compression
image quality.
.SH COMPRESSION FILE FORMATS
The following formats are supported as compression inputs:
.PP
.RS 8
LDR Formats:
.IP \(bu
BMP (*.bmp)
.IP \(bu
PNG (*.png)
.IP \(bu
Targa (*.tga)
.IP \(bu
JPEG (*.jpg)
.PP
HDR Formats:
.IP \(bu
OpenEXR (*.exr)
.IP \(bu
Radiance HDR (*.hdr)
.PP
Container Formats:
.IP \(bu
Khronos Texture KTX (*.ktx)
.IP \(bu
DirectDraw Surface DDS (*.dds)
.RE
.PP
For the KTX and DDS formats only a subset of the features of the
formats are supported:
.IP \(bu
Texture topology must be 2D, 2D-array, 3D, or cube-map. Note
that 2D-array textures are treated as 3D block input.
.IP \(bu
Texel format must be R, RG, RGB, BGR, RGBA, BGRA, L, or LA.
.IP \(bu
Only the first mipmap in the file will be read.
.PP
The following formats are supported as compression outputs:
.IP \(bu
ASTC (*.astc)
.IP \(bu
Khronos Texture KTX (*.ktx)
.SH DECOMPRESSION FILE FORMATS
The following formats are supported as decompression inputs:
.IP \(bu
ASTC (*.astc)
.IP \(bu
Khronos Texture KTX (*.ktx)
.PP
The following formats are supported as decompression outputs:
.PP
.RS 8
LDR Formats:
.IP \(bu
BMP (*.bmp)
.IP \(bu
PNG (*.png)
.IP \(bu
Targa (*.tga)
.PP
HDR Formats:
.IP \(bu
OpenEXR (*.exr)
.IP \(bu
Radiance HDR (*.hdr)
.PP
Container Formats:
.IP \(bu
Khronos Texture KTX (*.ktx)
.IP \(bu
DirectDraw Surface DDS (*.dds)
.RE
